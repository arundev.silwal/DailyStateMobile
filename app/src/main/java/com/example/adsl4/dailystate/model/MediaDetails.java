
package com.example.adsl4.dailystate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MediaDetails {

    @SerializedName("sizes")
    @Expose
    private Sizes sizes;

    public Sizes getSizes() {
        return sizes;
    }

    public void setSizes(Sizes sizes) {
        this.sizes = sizes;
    }

}

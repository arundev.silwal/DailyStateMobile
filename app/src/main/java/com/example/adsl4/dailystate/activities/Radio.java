package com.example.adsl4.dailystate.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.services.RadioService;

public class Radio extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Button btnPlay, btnStop;
    SeekBar seekVolume;
    AudioManager audioManager;
    TextView txtPer;
    int maxVolume, curVolume;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private int notification_id=1;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        
        
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        btnPlay = findViewById(R.id.btnPlay);
        btnStop = findViewById(R.id.btnStop);
        txtPer=findViewById(R.id.txtPer);
        seekVolume = findViewById(R.id.seekVolume);

        context=this;
        final int radioNotification=notification_id+1;
        notificationManager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent button_intent= new Intent("Button_clicked");
        button_intent.putExtra("id",radioNotification);

        PendingIntent p_button_intent= PendingIntent.getBroadcast(context,123,button_intent,0);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startService(new Intent(Radio.this, RadioService.class));
                btnPlay.setText("Playing");
                btnPlay.setEnabled(false);
                Intent notification_intent=new Intent(context,Radio.class);
                PendingIntent pendingIntent=PendingIntent.getActivity(context,0,notification_intent,0);

                builder=new NotificationCompat.Builder(context,"hello");
                builder.setSmallIcon(R.drawable.ic_radio_black_24dp)
                        .setAutoCancel(false)
                        .setContentTitle("Radio Gorkhali")
                        .setContentText("Playing...")
                        .setOngoing(true)
                        .setContentIntent(pendingIntent);
                notificationManager.notify(radioNotification,builder.build());
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(Radio.this, RadioService.class));
                btnPlay.setEnabled(true);
                btnPlay.setText("Play");
                notificationManager.cancel(radioNotification);
            }
        });

        seekVolume.setMax(maxVolume);
        seekVolume.setProgress(curVolume);
        seekVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                txtPer.setText("" + progress*10);
                txtPer.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            int index = seekVolume.getProgress();
            seekVolume.setProgress(index + 1);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            int index = seekVolume.getProgress();
            seekVolume.setProgress(index - 1);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        super.onBackPressed();
    }

    @Override
    protected void onPostResume() {
        SharedPreferences sharedPreferences  =Radio.this.getSharedPreferences("radio", Context.MODE_PRIVATE);
        int a=sharedPreferences.getInt("rad",0);
        if (a==1){
            btnPlay.setText("Playing");
            btnPlay.setEnabled(false);
        }
        else {
            btnPlay.setText("Play");
            btnPlay.setEnabled(true);
        }
        super.onPostResume();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_radio:

                break;
            case R.id.nav_home:
                Intent home = new Intent(Radio.this, Home.class);
                startActivity(home);
                break;
            case R.id.nav_news: {
                Intent ni = new Intent(Radio.this, CategoryPage.class);
                ni.putExtra("cat", "समाचार");
                startActivity(ni);
                break;
            }
            case R.id.nav_national: {
                Intent nati = new Intent(Radio.this, CategoryPage.class);
                nati.putExtra("cat", "राष्ट्रिय");
                startActivity(nati);
                break;
            }
            case R.id.nav_biz:
                Intent bi = new Intent(Radio.this, CategoryPage.class);
                bi.putExtra("cat", "अर्थ");
                startActivity(bi);
                break;
            case R.id.nav_thoughts:
                Intent ti = new Intent(Radio.this, CategoryPage.class);
                ti.putExtra("cat", "विचार");
                startActivity(ti);
                break;
            case R.id.nav_talks:
                Intent tksi = new Intent(Radio.this, CategoryPage.class);
                tksi.putExtra("cat", "अन्तरवार्ता");
                startActivity(tksi);
                break;
            case R.id.nav_world:
                Intent wi = new Intent(Radio.this, CategoryPage.class);
                wi.putExtra("cat", "अन्तराष्ट्रिय");
                startActivity(wi);
                break;
            case R.id.nav_sports:
                Intent si = new Intent(Radio.this, CategoryPage.class);
                si.putExtra("cat", "खेलकुद");
                startActivity(si);
                break;
            case R.id.nav_entertainment:
                Intent ei = new Intent(Radio.this, CategoryPage.class);
                ei.putExtra("cat", "मनोरन्जन");
                startActivity(ei);break;
            case R.id.nav_tech:
                Intent teci = new Intent(Radio.this, CategoryPage.class);
                teci.putExtra("cat", "प्रविधि");
                startActivity(teci);
                break;
//            case R.id.nav_migrants:
//                Intent mi = new Intent(Radio.this, CategoryPage.class);
//                mi.putExtra("cat", "प्रवासी-समाचार");
//                startActivity(mi);
//                break;
            case R.id.nav_health:
                Intent hi = new Intent(Radio.this, CategoryPage.class);
                hi.putExtra("cat", "स्वास्थ्य");
                startActivity(hi);
                break;
//            case R.id.nav_share:
//
//                break;
//            case R.id.nav_send:
//
//                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

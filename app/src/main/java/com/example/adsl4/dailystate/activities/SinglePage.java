package com.example.adsl4.dailystate.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.apis.DailystateApi;
import com.example.adsl4.dailystate.apis.EconomyApi;
import com.example.adsl4.dailystate.apis.EntertainmentApi;
import com.example.adsl4.dailystate.apis.HeadlinesApi;
import com.example.adsl4.dailystate.apis.HealthApi;
import com.example.adsl4.dailystate.apis.HoroscopeApi;
import com.example.adsl4.dailystate.apis.InternationalApi;
import com.example.adsl4.dailystate.apis.InterviewsApi;
import com.example.adsl4.dailystate.apis.LatestApi;
import com.example.adsl4.dailystate.apis.MigrantsApi;
import com.example.adsl4.dailystate.apis.NationalApi;
import com.example.adsl4.dailystate.apis.SportsApi;
import com.example.adsl4.dailystate.apis.TechnologyApi;
import com.example.adsl4.dailystate.apis.ThoughtsApi;
import com.example.adsl4.dailystate.model.DailyState;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SinglePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private static final String BASE_URL = "http://dailystatenews.com/wp-json/wp/v2/";
    ImageView imgView;
    WebView web, webTitle, webDate;
    ProgressBar pb;
    Integer fetMedia;
    String fetMediaURL,Category;
    Matrix matrix=new Matrix();
    ScaleGestureDetector scaleGestureDetector;
    float scale=1f;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_page);
        webTitle = findViewById(R.id.webNewsTitle);
        webDate = findViewById(R.id.txtDate);
        imgView = findViewById(R.id.fimage);
        web = findViewById(R.id.webContent);
        pb = findViewById(R.id.pbHeaderProgress);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        
        WebSettings settings = web.getSettings();
        settings.setTextZoom(settings.getTextZoom() + 25);
        web.getSettings().setBuiltInZoomControls(true);
        web.getSettings().setDisplayZoomControls(true);
        settings.setJavaScriptEnabled(true);
        web.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        web.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        web.setWebChromeClient(new WebChromeClient());
        web.getSettings().setPluginState(WebSettings.PluginState.ON);
        web.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        web.setWebViewClient(new WebViewClient());
        web.setHorizontalScrollBarEnabled(false);
        WebSettings settings1 = webTitle.getSettings();
        settings1.setTextZoom(settings1.getTextZoom() + 50);
        scaleGestureDetector=new ScaleGestureDetector(this,new ScaleListener());
        isNetworkAvailable();

    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener{
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale=scale * detector.getScaleFactor();
            scale=Math.max(0.1f,Math.min(scale,5f));
            matrix.setScale(scale, scale);
            imgView.setImageMatrix(matrix);
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        super.onBackPressed();
        SharedPreferences sharedPreferences  =getSharedPreferences("category", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("catSingle",Category);
        editor.apply();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        super.onBackPressed();
    }

    public void retroClass() {
        Intent intent = getIntent();
        Category = intent.getStringExtra("cat");
        final int pos = intent.getIntExtra("pos", 0);
        this.setTitle(Category);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        switch (Category) {
            case "समाचार":
                SharedPreferences sharedPreferences  =getSharedPreferences("category", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("cat","समाचार");
                editor.apply();
                final DailystateApi dailystateApi = retrofit.create(DailystateApi.class);
                Call<List<DailyState>> hello = dailystateApi.getData();
                hello.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()).into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "राष्ट्रिय":
                final NationalApi nationalApi = retrofit.create(NationalApi.class);
                Call<List<DailyState>> national = nationalApi.getData();
                national.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अर्थ":
                final EconomyApi economyApi = retrofit.create(EconomyApi.class);
                Call<List<DailyState>> economy = economyApi.getData();
                economy.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "विचार":
                final ThoughtsApi thoughtsApi = retrofit.create(ThoughtsApi.class);
                Call<List<DailyState>> thoughts = thoughtsApi.getData();
                thoughts.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अन्तरवार्ता":
                final InterviewsApi interviewsApi = retrofit.create(InterviewsApi.class);
                Call<List<DailyState>> interview = interviewsApi.getData();
                interview.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अन्तराष्ट्रिय":
                final InternationalApi internationalApi = retrofit.create(InternationalApi.class);
                Call<List<DailyState>> international = internationalApi.getData();
                international.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "खेलकुद":
                final SportsApi sportsApi = retrofit.create(SportsApi.class);
                Call<List<DailyState>> sports = sportsApi.getData();
                sports.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "मनोरन्जन":
                final EntertainmentApi entertainmentApi = retrofit.create(EntertainmentApi.class);
                Call<List<DailyState>> entertainment = entertainmentApi.getData();
                entertainment.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "प्रविधि":
                final TechnologyApi technologyApi = retrofit.create(TechnologyApi.class);
                Call<List<DailyState>> technology = technologyApi.getData();
                technology.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "प्रवासी-समाचार":
                final MigrantsApi migrantsApi = retrofit.create(MigrantsApi.class);
                Call<List<DailyState>> migrants = migrantsApi.getData();
                migrants.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "स्वास्थ्य":
                final HealthApi healthApi = retrofit.create(HealthApi.class);
                Call<List<DailyState>> health = healthApi.getData();
                health.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()) .into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "पछिल्लो समाचार":
                this.setTitle(getString(R.string.tab_text_1));
                final LatestApi latestApi = retrofit.create(LatestApi.class);
                Call<List<DailyState>> latest = latestApi.getData();
                latest.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()).into(imgView);
                            try {
                                fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            }
                            catch (Exception e){
                                fetMediaURL = "http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                            }
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "राशिफल":
                this.setTitle(getString(R.string.tab_text_3));
                final HoroscopeApi horoscopeApi = retrofit.create(HoroscopeApi.class);
                Call<List<DailyState>> horoscope = horoscopeApi.getData();
                horoscope.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()).into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "मुख्य समाचार":
                this.setTitle(getString(R.string.tab_text_2));
                final HeadlinesApi headlinesApi = retrofit.create(HeadlinesApi.class);
                Call<List<DailyState>> headline = headlinesApi.getData();
                headline.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        for (int i = 0; i < content.size(); i++) {
                            Picasso.with(getApplicationContext()).load(content.get(pos).getBetterFeaturedImage().getSourceUrl()).into(imgView);
                            fetMediaURL = content.get(pos).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                            fetMedia = content.get(pos).getFeaturedMedia();
                            String bodyHTML = "";
                            String Title = "<html>" + content.get(pos).getTitle().getRendered() + "<body style='text-align: justify; font-weight: bold;'>" + bodyHTML + "</body></html>";
                            String Content = "<html>" + content.get(pos).getContent().getRendered() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            String Date = "<html>" + content.get(pos).getDate() + "<body style='text-align: justify;'>" + bodyHTML + "</body></html>";
                            webTitle.loadData(Title, "text/html; charset=UTF-8", null);
                            webDate.loadData(Date, "text/html; charset=UTF-8", null);
                            web.loadData(Content, "text/html; charset=UTF-8", null);
                            pb.setVisibility(View.GONE);
                        }
                        //txtTest.setText(fetMediaURL);

                        web.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(SinglePage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_radio:
                Intent ri = new Intent(SinglePage.this, Radio.class);
                startActivity(ri);
                break;
            case R.id.nav_home:
                Intent home = new Intent(SinglePage.this, Home.class);
                startActivity(home);
                break;
            case R.id.nav_news: {
                Intent ni = new Intent(SinglePage.this, CategoryPage.class);
                ni.putExtra("cat", "समाचार");
                startActivity(ni);
                break;
            }
            case R.id.nav_national: {
                Intent nati = new Intent(SinglePage.this, CategoryPage.class);
                nati.putExtra("cat", "राष्ट्रिय");
                startActivity(nati);
                break;
            }
            case R.id.nav_biz:
                Intent bi = new Intent(SinglePage.this, CategoryPage.class);
                bi.putExtra("cat", "अर्थ");
                startActivity(bi);
                break;
            case R.id.nav_thoughts:
                Intent ti = new Intent(SinglePage.this, CategoryPage.class);
                ti.putExtra("cat", "विचार");
                startActivity(ti);
                break;
            case R.id.nav_talks:
                Intent tksi = new Intent(SinglePage.this, CategoryPage.class);
                tksi.putExtra("cat", "अन्तरवार्ता");
                startActivity(tksi);
                break;
            case R.id.nav_world:
                Intent wi = new Intent(SinglePage.this, CategoryPage.class);
                wi.putExtra("cat", "अन्तराष्ट्रिय");
                startActivity(wi);
                break;
            case R.id.nav_sports:
                Intent si = new Intent(SinglePage.this, CategoryPage.class);
                si.putExtra("cat", "खेलकुद");
                startActivity(si);
                break;
            case R.id.nav_entertainment:
                Intent ei = new Intent(SinglePage.this, CategoryPage.class);
                ei.putExtra("cat", "मनोरन्जन");
                startActivity(ei);break;
            case R.id.nav_tech:
                Intent teci = new Intent(SinglePage.this, CategoryPage.class);
                teci.putExtra("cat", "प्रविधि");
                startActivity(teci);
                break;
//            case R.id.nav_migrants:
//                Intent mi = new Intent(SinglePage.this, CategoryPage.class);
//                mi.putExtra("cat", "प्रवासी-समाचार");
//                startActivity(mi);
//                break;
            case R.id.nav_health:
                Intent hi = new Intent(SinglePage.this, CategoryPage.class);
                hi.putExtra("cat", "स्वास्थ्य");
                startActivity(hi);
                break;
//            case R.id.nav_share:
//
//                break;
//            case R.id.nav_send:
//
//                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private boolean isNetworkAvailable() {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null)
        {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.splash)
                    .setTitle("Daily State")
                    .setMessage("No Internet Connection!!!")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            onBackPressed();
                        }
                    })
                    .show();
            return false;
        }
        else
        {
            if(info.isConnected())
            {
                retroClass();
                return true;
            }
            else
            {
//                Log.d(TAG," internet connection");
                return true;
            }

        }
    }

}


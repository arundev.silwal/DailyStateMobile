
package com.example.adsl4.dailystate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sizes {

    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;


    public Thumbnail getThumbnail() {
        return thumbnail;
    }
}

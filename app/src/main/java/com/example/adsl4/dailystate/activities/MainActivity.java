package com.example.adsl4.dailystate.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.apis.LatestApi;
import com.example.adsl4.dailystate.model.DailyState;
import com.example.adsl4.dailystate.recModelLatest.ListItemLatest;
import com.example.adsl4.dailystate.recModelLatest.ResAdapterLatest;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG="MainActivity";
    private static final String BASE_URL="http://dailystatenews.com/wp-json/wp/v2/";
    private RecyclerView.Adapter adapter;
    private List<ListItemLatest> listItemLatest;
    private RecyclerView recyclerView;
    public ImageView imgView;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView=findViewById(R.id.recLatest);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        imgView=findViewById(R.id.catImg);
        pb=findViewById(R.id.pbLatest);

        this.setTitle("Daily State: गृहपृष्ठ");

        FloatingActionButton fab = findViewById(R.id.fabMain);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,Radio.class);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        retroClass();
    }
    public void retroClass(){
        SharedPreferences sharedPreferences  =getSharedPreferences("category", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("cat","भर्खरै प्रकाशित");
        editor.apply();
        final Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        final LatestApi latestApi = retrofit.create(LatestApi.class);
        Call<List<DailyState>> hello = latestApi.getData();
        hello.enqueue(new Callback<List<DailyState>>() {
            @Override
            public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                List<DailyState> content = response.body();
                listItemLatest = new ArrayList<>();
                String catImg = null;
                try {
                    for (int i = 0; i < content.size(); i++) {
                        try {
                            catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                        }
                        catch (Exception e){
                            catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                        }
                        String catTitle = Html.fromHtml(content.get(i).getTitle().getRendered()).toString();
                        ListItemLatest item = new ListItemLatest(catTitle, catImg);
                        listItemLatest.add(item);
                    }
                    adapter = new ResAdapterLatest(listItemLatest, getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    pb.setVisibility(View.GONE);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                } catch (Exception e) {
                    //Toast.makeText(MainActivity.this, "hello: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d(TAG, "error "+e.getMessage());
                    pb.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<DailyState>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.splash)
                    .setTitle("Daily State")
                    .setMessage("You are about to leave me!!!")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MainActivity.this.finish();
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("Sorry", null)
                    .show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_radio:
                Intent ri = new Intent(MainActivity.this, Radio.class);
                startActivity(ri);
               overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_home:

                break;
            case R.id.nav_news: {
                Intent ni = new Intent(MainActivity.this, CategoryPage.class);
                ni.putExtra("cat", "समाचार");
                startActivity(ni);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            }
            case R.id.nav_national: {
                Intent nati = new Intent(MainActivity.this, CategoryPage.class);
                nati.putExtra("cat", "राष्ट्रिय");
                startActivity(nati);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            }
            case R.id.nav_biz:
                Intent bi = new Intent(MainActivity.this, CategoryPage.class);
                bi.putExtra("cat", "अर्थ");
                startActivity(bi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_thoughts:
                Intent ti = new Intent(MainActivity.this, CategoryPage.class);
                ti.putExtra("cat", "विचार");
                startActivity(ti);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_talks:
                Intent tksi = new Intent(MainActivity.this, CategoryPage.class);
                tksi.putExtra("cat", "अन्तरवार्ता");
                startActivity(tksi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_world:
                Intent wi = new Intent(MainActivity.this, CategoryPage.class);
                wi.putExtra("cat", "अन्तराष्ट्रिय");
                startActivity(wi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_sports:
                Intent si = new Intent(MainActivity.this, CategoryPage.class);
                si.putExtra("cat", "खेलकुद");
                startActivity(si);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_entertainment:
                Intent ei = new Intent(MainActivity.this, CategoryPage.class);
                ei.putExtra("cat", "मनोरन्जन");
                startActivity(ei);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_tech:
                Intent teci = new Intent(MainActivity.this, CategoryPage.class);
                teci.putExtra("cat", "प्रविधि");
                startActivity(teci);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
//            case R.id.nav_migrants:
//                Intent mi = new Intent(MainActivity.this, CategoryPage.class);
//                mi.putExtra("cat", "प्रवासी-समाचार");
//                startActivity(mi);
//                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
//                break;
            case R.id.nav_health:
                Intent hi = new Intent(MainActivity.this, CategoryPage.class);
                hi.putExtra("cat", "स्वास्थ्य");
                startActivity(hi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
//            case R.id.nav_share:
//
//                break;
//            case R.id.nav_send:
//
//                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

package com.example.adsl4.dailystate.recModelCategory;

/**
 * Created by adsl4 on 11/28/17.
 */

public class ListItem {

    private String head;
    private String catImg;

    public String getHead() {
        return head;
    }

    public String getCatImg() {
        return catImg;
    }

    public ListItem(String head, String catImg) {
        this.head = head;
        this.catImg = catImg;
    }
}

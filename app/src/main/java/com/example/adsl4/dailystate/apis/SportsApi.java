package com.example.adsl4.dailystate.apis;

import com.example.adsl4.dailystate.model.DailyState;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by adsl4 on 11/29/17.
 */

public interface SportsApi {
    String BASE_URL="http://dailystatenews.com/wp-json/wp/v2/";
    @Headers("Content-Type: application/json")
    @GET("posts?categories=15&per_page=100")
    Call<List<DailyState>> getData();
}

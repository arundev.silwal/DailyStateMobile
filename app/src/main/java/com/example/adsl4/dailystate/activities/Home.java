package com.example.adsl4.dailystate.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.fragments.Headline;
import com.example.adsl4.dailystate.fragments.Horoscope;
import com.example.adsl4.dailystate.fragments.Latest;

import org.apache.commons.io.FileUtils;

import java.io.File;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout =  findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mViewPager.setOffscreenPageLimit(3);

        FloatingActionButton fab =  findViewById(R.id.fabHome);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Home.this,Radio.class);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        this.setTitle("hello");


    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home, container, false);
            return rootView;
        }
    }

    /**`
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
           switch (position){
               case 0:
                   Latest latest=new Latest();
                   return latest;
               case 1:
                   Headline headline=new Headline();
                   return headline;
               case 2:
                   Horoscope horoscope=new Horoscope();
                   return horoscope;
           }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
  

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_radio:
                Intent ri = new Intent(Home.this, Radio.class);
                startActivity(ri);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_home:

                break;
            case R.id.nav_news: {
                Intent ni = new Intent(Home.this, CategoryPage.class);
                ni.putExtra("cat", "समाचार");
                startActivity(ni);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            }
            case R.id.nav_national: {
                Intent nati = new Intent(Home.this, CategoryPage.class);
                nati.putExtra("cat", "राष्ट्रिय");
                startActivity(nati);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            }
            case R.id.nav_biz:
                Intent bi = new Intent(Home.this, CategoryPage.class);
                bi.putExtra("cat", "अर्थ");
                startActivity(bi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_thoughts:
                Intent ti = new Intent(Home.this, CategoryPage.class);
                ti.putExtra("cat", "विचार");
                startActivity(ti);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_talks:
                Intent tksi = new Intent(Home.this, CategoryPage.class);
                tksi.putExtra("cat", "अन्तरवार्ता");
                startActivity(tksi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_world:
                Intent wi = new Intent(Home.this, CategoryPage.class);
                wi.putExtra("cat", "अन्तराष्ट्रिय");
                startActivity(wi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_sports:
                Intent si = new Intent(Home.this, CategoryPage.class);
                si.putExtra("cat", "खेलकुद");
                startActivity(si);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_entertainment:
                Intent ei = new Intent(Home.this, CategoryPage.class);
                ei.putExtra("cat", "मनोरन्जन");
                startActivity(ei);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.nav_tech:
                Intent teci = new Intent(Home.this, CategoryPage.class);
                teci.putExtra("cat", "प्रविधि");
                startActivity(teci);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
//            case R.id.nav_migrants:
//                Intent mi = new Intent(Home.this, CategoryPage.class);
//                mi.putExtra("cat", "प्रवासी-समाचार");
//                startActivity(mi);
//                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
//                break;
            case R.id.nav_health:
                Intent hi = new Intent(Home.this, CategoryPage.class);
                hi.putExtra("cat", "स्वास्थ्य");
                startActivity(hi);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
//            case R.id.nav_share:
//
//                break;
//            case R.id.nav_send:
//
//                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.splash)
                    .setTitle("Daily State")
                    .setMessage("You are about to leave me!!!")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            FileUtils.deleteQuietly(getApplicationContext().getCacheDir());
                            clearApplicationData();
                            Home.this.finish();
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("Sorry", null)
                    .show();
        }
    }
     public void clearApplicationData() {
            File cache = getCacheDir();
            File appDir = new File(cache.getParent());
            if(appDir.exists()){
                String[] children = appDir.list();
                for(String s : children){
                    if(!s.equals("lib")){
                        deleteDir(new File(appDir, s));
                        Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                    }
                }
            }
        }

        public static boolean deleteDir(File dir) {
            if (dir != null && dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }

            return dir.delete();
        }
}

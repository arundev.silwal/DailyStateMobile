package com.example.adsl4.dailystate.recModelLatest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.activities.SinglePage;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by adsl4 on 11/28/17.
 */

public class ResAdapterLatest extends RecyclerView.Adapter<ResAdapterLatest.ViewHolder> {

    private List<ListItemLatest> listItems;
    private Context context;

    public ResAdapterLatest(List<ListItemLatest> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,  int position) {
        final int posn=position;
        final ListItemLatest listItem=listItems.get(position);
        holder.txtHead.setText(listItem.getHead());
        Picasso.with(context).load(listItem.getCatImg()).into(holder.catImg);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, SinglePage.class);
                i.putExtra("cat","पछिल्लो समाचार");
                i.putExtra("pos",posn);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtHead;
        public ImageView catImg;
        public LinearLayout linearLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            txtHead=itemView.findViewById(R.id.catHead);
            catImg=itemView.findViewById(R.id.catImg);
            linearLayout =itemView.findViewById(R.id.linearLayout);
        }
    }

}

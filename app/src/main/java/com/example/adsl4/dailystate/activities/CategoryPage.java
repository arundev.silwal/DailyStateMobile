package com.example.adsl4.dailystate.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.apis.DailystateApi;
import com.example.adsl4.dailystate.apis.EconomyApi;
import com.example.adsl4.dailystate.apis.EntertainmentApi;
import com.example.adsl4.dailystate.apis.HealthApi;
import com.example.adsl4.dailystate.apis.InternationalApi;
import com.example.adsl4.dailystate.apis.InterviewsApi;
import com.example.adsl4.dailystate.apis.MigrantsApi;
import com.example.adsl4.dailystate.apis.NationalApi;
import com.example.adsl4.dailystate.apis.SportsApi;
import com.example.adsl4.dailystate.apis.TechnologyApi;
import com.example.adsl4.dailystate.apis.ThoughtsApi;
import com.example.adsl4.dailystate.model.DailyState;
import com.example.adsl4.dailystate.recModelCategory.ListItem;
import com.example.adsl4.dailystate.recModelCategory.ResAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CategoryPage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public float totSize;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListItem> listItems;
    private static final String BASE_URL="http://dailystatenews.com/wp-json/wp/v2/";
    public ImageView imgView;
    String  catName;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_page);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        recyclerView=findViewById(R.id.recCategory);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        imgView=findViewById(R.id.catImg);
        pb=findViewById(R.id.pbCategory);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {

            } else {
                catName= extras.getString("cat");
            }
        } else {
            catName= (String) savedInstanceState.getSerializable("cat");
            SharedPreferences sharedPreferences  =getApplicationContext().getSharedPreferences("category", Context.MODE_PRIVATE);
            catName=sharedPreferences.getString("catSingle","");
        }
        
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        retroCatClass();
        this.setTitle(catName);
    }

    private void retroCatClass() {
        final Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        SharedPreferences sharedPreferences  =getSharedPreferences("category", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("cat",catName);
        editor.apply();
        switch (catName) {
            case "समाचार":
                final DailystateApi dailystateApi = retrofit.create(DailystateApi.class);
                Call<List<DailyState>> hello = dailystateApi.getData();
                hello.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "राष्ट्रिय":
                final NationalApi nationalApi = retrofit.create(NationalApi.class);
                Call<List<DailyState>> national = nationalApi.getData();
                national.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अर्थ":
                final EconomyApi economyApi = retrofit.create(EconomyApi.class);
                Call<List<DailyState>> economy = economyApi.getData();
                economy.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        int j=0;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                                j=i;
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " +j+"hhjhjhj"+ e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "विचार":
                final ThoughtsApi thoughtsApi = retrofit.create(ThoughtsApi.class);
                Call<List<DailyState>> thoughts = thoughtsApi.getData();
                thoughts.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    continue;
                                }
                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अन्तरवार्ता":
                final InterviewsApi interviewsApi = retrofit.create(InterviewsApi.class);
                Call<List<DailyState>> interview = interviewsApi.getData();
                interview.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "अन्तराष्ट्रिय":
                final InternationalApi internationalApi = retrofit.create(InternationalApi.class);
                Call<List<DailyState>> international = internationalApi.getData();
                international.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "खेलकुद":
                final SportsApi sportsApi = retrofit.create(SportsApi.class);
                Call<List<DailyState>> sports = sportsApi.getData();
                sports.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "मनोरन्जन":
                final EntertainmentApi entertainmentApi = retrofit.create(EntertainmentApi.class);
                Call<List<DailyState>> entertainment = entertainmentApi.getData();
                entertainment.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "प्रविधि":
                final TechnologyApi technologyApi = retrofit.create(TechnologyApi.class);
                Call<List<DailyState>> technology = technologyApi.getData();
                technology.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "प्रवासी-समाचार":
                final MigrantsApi migrantsApi = retrofit.create(MigrantsApi.class);
                Call<List<DailyState>> migrants = migrantsApi.getData();
                migrants.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);

                    }
                });
                break;
            case "स्वास्थ्य":
                final HealthApi healthApi = retrofit.create(HealthApi.class);
                Call<List<DailyState>> health = healthApi.getData();
                health.enqueue(new Callback<List<DailyState>>() {
                    @Override
                    public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                        List<DailyState> content = response.body();
                        totSize = content.size();
                        String catImg = null;
                        listItems = new ArrayList<>();
                        try {
                            for (int i = 0; i < content.size(); i++) {
                                try {
                                    catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                                }
                                catch (Exception e){
                                    catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                                }                                String catTitle = Html.fromHtml(Html.fromHtml(content.get(i).getTitle().getRendered()).toString()).toString();
                                ListItem item = new ListItem(catTitle, catImg);
                                listItems.add(item);
                            }
                            adapter = new ResAdapter(listItems, getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pb.setVisibility(View.GONE);

                        } catch (Exception e) {
                            Toast.makeText(CategoryPage.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            pb.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DailyState>> call, Throwable t) {
                        Toast.makeText(CategoryPage.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                        pb.setVisibility(View.GONE);
                    }
                });
                break;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        String name;
        SharedPreferences sharedPreferences  =getApplicationContext().getSharedPreferences("category", Context.MODE_PRIVATE);
        name=sharedPreferences.getString("cat","");
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_radio:
                Intent ri = new Intent(CategoryPage.this, Radio.class);
                startActivity(ri);
                break;
            case R.id.nav_home:
                Intent home = new Intent(CategoryPage.this, Home.class);
                startActivity(home);
                break;
            case R.id.nav_news: {
                if (name.equals("समाचार")){

                }
                else {
                    Intent ni = new Intent(CategoryPage.this, CategoryPage.class);
                    ni.putExtra("cat", "समाचार");
                    startActivity(ni);
                }
                break;
            }
            case R.id.nav_national: {
                if (name.equals("राष्ट्रिय")){

                }
                else {
                    Intent nati = new Intent(CategoryPage.this, CategoryPage.class);
                    nati.putExtra("cat", "राष्ट्रिय");
                    startActivity(nati);
                }
                break;
            }
            case R.id.nav_biz:
                if (name.equals("अर्थ")){

                }
                else {
                    Intent bi = new Intent(CategoryPage.this, CategoryPage.class);
                    bi.putExtra("cat", "अर्थ");
                    startActivity(bi);
                }
                break;
            case R.id.nav_thoughts:
                if (name.equals("विचार")){

                }
                else {
                    Intent ti = new Intent(CategoryPage.this, CategoryPage.class);
                    ti.putExtra("cat", "विचार");
                    startActivity(ti);
                }
                break;
            case R.id.nav_talks:
                if (name.equals("अन्तरवार्ता")){

                }
                else {
                    Intent tksi = new Intent(CategoryPage.this, CategoryPage.class);
                    tksi.putExtra("cat", "अन्तरवार्ता");
                    startActivity(tksi);
                }
                break;
            case R.id.nav_world:
                if (name.equals("अन्तराष्ट्रिय")){

                }
                else {
                    Intent wi = new Intent(CategoryPage.this, CategoryPage.class);
                    wi.putExtra("cat", "अन्तराष्ट्रिय");
                    startActivity(wi);
                }
                break;
            case R.id.nav_sports:
                if (name.equals("खेलकुद")){

                }
                else {
                    Intent si = new Intent(CategoryPage.this, CategoryPage.class);
                    si.putExtra("cat", "खेलकुद");
                    startActivity(si);
                }
                break;
            case R.id.nav_entertainment:
                if (name.equals("मनोरन्जन")){

                }
                else {
                    Intent ei = new Intent(CategoryPage.this, CategoryPage.class);
                    ei.putExtra("cat", "मनोरन्जन");
                    startActivity(ei);
                }
                    break;
            case R.id.nav_tech:
                if (name.equals("प्रविधि")){

                }
                else {
                    Intent teci = new Intent(CategoryPage.this, CategoryPage.class);
                    teci.putExtra("cat", "प्रविधि");
                    startActivity(teci);
                }
                break;
//            case R.id.nav_migrants:
//                if (name.equals("प्रवासी-समाचार")){
//
//                }
//                else {
//                    Intent mi = new Intent(CategoryPage.this, CategoryPage.class);
//                    mi.putExtra("cat", "प्रवासी-समाचार");
//                    startActivity(mi);
//                }
//                break;
            case R.id.nav_health:
                if (name.equals("स्वास्थ्य")){

                }
                else {
                    Intent hi = new Intent(CategoryPage.this, CategoryPage.class);
                    hi.putExtra("cat", "स्वास्थ्य");
                    startActivity(hi);
                }
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}


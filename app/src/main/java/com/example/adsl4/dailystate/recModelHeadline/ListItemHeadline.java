package com.example.adsl4.dailystate.recModelHeadline;

/**
 * Created by adsl4 on 11/28/17.
 */

public class ListItemHeadline {

    private String head;
    private String catImg;

    public String getHead() {
        return head;
    }

    public String getCatImg() {
        return catImg;
    }

    public ListItemHeadline(String head, String catImg) {
        this.head = head;
        this.catImg = catImg;
    }
}

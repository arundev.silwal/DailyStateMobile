package com.example.adsl4.dailystate.services;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by adsl4 on 12/5/17.
 */

public class RadioService extends Service {
    final MediaPlayer mediaPlayer = new MediaPlayer();
    NotificationCompat.Builder notification;
    private static final int   notification_id = (int) System.currentTimeMillis();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

       if (mediaPlayer.isPlaying()){

       }
       else {
           mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
           try {
               mediaPlayer.setDataSource("http://216.55.141.189:8152/");
           } catch (IOException e) {
               Toast.makeText(getApplicationContext(), "error " + e.getMessage(), Toast.LENGTH_LONG).show();
               e.printStackTrace();
           }
           try {
               mediaPlayer.prepare();
               mediaPlayer.start();
               SharedPreferences sharedPreferences = RadioService.this.getSharedPreferences("radio", Context.MODE_PRIVATE);
               SharedPreferences.Editor editor = sharedPreferences.edit();
               editor.putInt("rad", 1);
               editor.apply();
//               NotificationManager mNotificationManager =
//                       (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            // The id of the channel.
//               String id = "my_channel_01";
//            // The user-visible name of the channel.
//               CharSequence name = "Test Channel";
//            // The user-visible description of the channel.
//               String description = "This is test channel";
//               int importance = NotificationManager.IMPORTANCE_HIGH;
//            // Configure the notification channel.
//               NotificationChannel mChannel = new NotificationChannel(id, name, importance);
//
//               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//                   mChannel.setDescription(description);
//                   mChannel.enableLights(true);
//                   // Sets the notification light color for notifications posted to this
//                   // channel, if the device supports this feature.
//                   mChannel.setLightColor(Color.RED);
//                   mChannel.enableVibration(true);
//                   mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
//                   mNotificationManager.createNotificationChannel(mChannel);
//               }
//               notification=new NotificationCompat.Builder(getApplicationContext(),mChannel.getId());
//               notification.setAutoCancel(true);
//               notification.setSmallIcon(R.drawable.ic_radio_black_24dp);
//               notification.setTicker("Playing Radio");
//               notification.setWhen(System.currentTimeMillis());
//               notification.setContentTitle("Radio");
//               notification.setContentText("Radio is playing");
//
//               Intent intent1=new Intent(getApplicationContext(), Radio.class);
//               PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
//               notification.setContentIntent(pendingIntent);
//               NotificationManager notificationManager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//               notificationManager.notify(notification_id,notification.build());

           } catch (IOException e) {
               e.printStackTrace();
           }

       }
        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        SharedPreferences sharedPreferences = RadioService.this.getSharedPreferences("radio", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("rad", 0);
        editor.apply();
        mediaPlayer.stop();
        mediaPlayer.reset();
        super.onDestroy();
    }
}

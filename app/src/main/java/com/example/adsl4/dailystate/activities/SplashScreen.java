package com.example.adsl4.dailystate.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.adsl4.dailystate.R;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT=1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        isNetworkAvailable();
    }
    private boolean isNetworkAvailable() {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null)
        {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.splash)
                    .setTitle("Daily State")
                    .setMessage("No Internet Connection!!!")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SplashScreen.this.finish();
                            System.exit(0);
                        }
                    })
                    .show();
            return false;
        }
        else
        {
            if(info.isConnected())
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent homeIntent=new Intent(SplashScreen.this,Home.class);
                        startActivity(homeIntent);
                        finish();
                    }
                },SPLASH_TIME_OUT);
                Toast.makeText(getApplicationContext(),"Connected",Toast.LENGTH_LONG).show();
                return true;
            }
            else
            {
//                Log.d(TAG," internet connection");
                return true;
            }

        }
    }
}

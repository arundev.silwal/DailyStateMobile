package com.example.adsl4.dailystate.apis;

import com.example.adsl4.dailystate.model.DailyState;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by adsl4 on 12/2/17.
 */

public interface LatestApi {
    String BASE_URL="http://dailystatenews.com/wp-json/wp/v2/";
    @Headers("Content-Type: application/json")
    @GET("posts?per_page=10")
    Call<List<DailyState>> getData();
}

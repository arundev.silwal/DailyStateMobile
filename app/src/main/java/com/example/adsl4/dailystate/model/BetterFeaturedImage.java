
package com.example.adsl4.dailystate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BetterFeaturedImage {


    private Integer post;
    @SerializedName("source_url")
    @Expose
    private String sourceUrl;
    @SerializedName("media_details")
    @Expose
    private MediaDetails mediaDetails;

    public MediaDetails getMediaDetails() {
        return mediaDetails;
    }

    public void setMediaDetails(MediaDetails mediaDetails) {
        this.mediaDetails = mediaDetails;
    }

    public Integer getPost() {
        return post;
    }

    public void setPost(Integer post) {
        this.post = post;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

}

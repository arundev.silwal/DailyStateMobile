package com.example.adsl4.dailystate.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.adsl4.dailystate.R;
import com.example.adsl4.dailystate.apis.HeadlinesApi;
import com.example.adsl4.dailystate.model.DailyState;
import com.example.adsl4.dailystate.recModelHeadline.ListItemHeadline;
import com.example.adsl4.dailystate.recModelHeadline.ResAdapterHeadline;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by adsl4 on 12/17/17.
 */

public class Headline extends Fragment
{
    private static final String BASE_URL="http://dailystatenews.com/wp-json/wp/v2/";
    private RecyclerView.Adapter adapter;
    private List<ListItemHeadline> listItemLatest;
    private RecyclerView recyclerView;
    public ImageView imgView;
    ProgressBar pb;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.headline,container,false);
        recyclerView=rootView.findViewById(R.id.recHeadline);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        imgView= rootView.findViewById(R.id.catImg);
        pb= rootView.findViewById(R.id.pbHeadline);
        retroClass();
        return rootView ;
    }

    public void retroClass(){
        SharedPreferences sharedPreferences  = getActivity().getSharedPreferences("category", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("cat","मुख्य समाचार");
        editor.apply();
        final Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        final HeadlinesApi headlinesApi = retrofit.create(HeadlinesApi.class);
        Call<List<DailyState>> hello = headlinesApi.getData();
        hello.enqueue(new Callback<List<DailyState>>() {
            @Override
            public void onResponse(Call<List<DailyState>> call, Response<List<DailyState>> response) {
                List<DailyState> content = response.body();
                listItemLatest = new ArrayList<>();
                String catImg = null;
                try {
                    for (int i = 0; i < content.size(); i++) {
                        try {
                            catImg = content.get(i).getBetterFeaturedImage().getMediaDetails().getSizes().getThumbnail().getSourceUrl();
                        }
                        catch (Exception e){
                            catImg="http://dailystatenews.com/wp-content/uploads/2017/11/Logo-for-statenews.png";
                        }
                        String catTitle = Html.fromHtml(content.get(i).getTitle().getRendered()).toString();
                        ListItemHeadline item = new ListItemHeadline(catTitle, catImg);
                        listItemLatest.add(item);
                    }
                    adapter = new ResAdapterHeadline(listItemLatest, getActivity().getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    pb.setVisibility(View.GONE);
                    getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                } catch (Exception e) {
                    //Toast.makeText(Home.this, "hello: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    pb.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<DailyState>> call, Throwable t) {

            }
        });
    }

}
